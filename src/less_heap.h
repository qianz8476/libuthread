/*
 * max_heap.h
 *
 *  Created on: Oct 21, 2013
 *      Author: qianz
 */

#ifndef MAX_HEAP_H_
#define MAX_HEAP_H_
#include <ucontext.h>
#include <time.h>

typedef struct heapArray queue;
typedef struct thread_info thread;

struct heapArray {
	thread **root;
	int size;
	int elements;
};

struct thread_info {
	int priority;
	struct timespec t;
	ucontext_t *ucp;
};

/*
 * Make a copy of the root of the heap. Replace root with the end of the array. Re-heapify
 * structure to keep min_heap properties.
 */
thread *pop_min(queue *q);

/*
 * Insert pointer to a thread structure in to the min_heap.
 * Dynamically expands/shrinks array to accommodate incoming threads.
 */
void insert(thread *node, queue *q);

/*
 * Initialize an array to hold pointers to thread structures.
 */
void init(queue *q);

/*
 * Recursively move nodes in the tree to keep min heap properties.
 */
void heapify(queue *q, int i);
#endif /* MAX_HEAP_H_ */
