/*
 * test.c
 *
 *  Created on: Oct 24, 2013
 *      Author: qianz
 */
#include "uthread.h"
#include <stdio.h>
#include <pthread.h>

int n_threads = 0;
int myid = 0;
pthread_mutex_t lk;
void do_something() {
	int id;
	pthread_mutex_lock(&lk);
	id = myid;
	myid++;
	printf("This is ult %d\n", id);
	if (n_threads < 20) {
		uthread_create(do_something, 1);
		n_threads++;
		uthread_create(do_something, 1);
		n_threads++;
	}
	printf("This is ult %d again\n", id);
	pthread_mutex_unlock(&lk);
	uthread_yield(2);
	pthread_mutex_lock(&lk);
	printf("This is ult %d one more time\n", id);
	pthread_mutex_unlock(&lk);
	uthread_exit();
}


int main() {
	system_init(1); //only 1 kernel thread
	pthread_mutex_init(&lk, NULL);
	uthread_create(do_something, 1);
	return 0;
}
