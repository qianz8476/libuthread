/*
 * uthread.c
 *
 *  Created on: Oct 20, 2013
 *      Author: qianz
 */
#include "uthread.h"
#include "less_heap.h"
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>

queue rq;
pthread_mutex_t queuelock;
pthread_mutex_t threadlock;
int max_num_threads;
int num_of_threads;

void system_init(int max_number_of_klt) {
	init(&rq);
	num_of_threads = 0;
	if(max_number_of_klt > 0)
		max_num_threads = max_number_of_klt;
	else
		max_num_threads = 1;
	pthread_mutex_init(&queuelock, NULL);
	pthread_mutex_init(&threadlock, NULL);
}

int uthread_create(void (*func)(), int pri_num) {
	pthread_mutex_lock(&threadlock);
	//Check if the max number of kernel threads have been reached.
	if(num_of_threads < max_num_threads) {
		//If not, immediately assign to kernel thread and run.
		void *child_stack = (void *)malloc(16384);
		child_stack += 16383;
		clone((int (*)(void *))func, child_stack, CLONE_VM|CLONE_FILES, NULL);
		num_of_threads++;
	}
	//Otherwise create thread structure and store into queue.
	else {
		//Initialize a thread structure.
		thread *th = (thread *)malloc(sizeof(thread));
		th->ucp = (ucontext_t *)malloc(sizeof(ucontext_t));
		getcontext(th->ucp);
		th->ucp->uc_stack.ss_sp = malloc(16384);
		th->ucp->uc_stack.ss_size = 16384;
		th->priority = pri_num;
		//Set func as th's context
		makecontext(th->ucp, func, 0);

		pthread_mutex_lock(&queuelock);
		insert(th, &rq);
		pthread_mutex_unlock(&queuelock);
	}
	pthread_mutex_unlock(&threadlock);
	return 0;
}

int uthread_yield(int pri_num) {
	//Lock queue and pop the highest priority thread.
	pthread_mutex_lock(&queuelock);
	thread *th = pop_min(&rq);
	pthread_mutex_unlock(&queuelock);

	//If a null pointer is returned, then there are no more threads to yield.
	//Return so current thread can continue executing.
	if(th == NULL) {
		return 0;
	}
	else {
		//Get context from thread structure.
		ucontext_t *newContext = th->ucp;
		//Free memory for thread structure.
		free(th);
		//Create structure to store current thread context.
		thread *curThread = (thread *)malloc(sizeof(thread));
		curThread->priority = pri_num;
		curThread->ucp = (ucontext_t *)malloc(sizeof(ucontext_t));
		getcontext(curThread->ucp);

		//Lock queue and insert current thread context with new priority.
		pthread_mutex_lock(&queuelock);
		insert(curThread, &rq);
		pthread_mutex_unlock(&queuelock);
		//Swap so that current thread state is saved and new thread context is run.
		swapcontext(curThread->ucp, newContext);
		return 0;
	}
}

void uthread_exit() {
	//Lock queue and pop the highest priority thread structure
	//based on the rules described in the header file.
	pthread_mutex_lock(&queuelock);
	thread *th = pop_min(&rq);
	pthread_mutex_unlock(&queuelock);

	//If a null pointer is returned, it implies the queue is empty.
	//Continue and decrement num_of_threads then exit thread.
	//Otherwise, retrieve context pointer before freeing thread structure
	//Then set current thread to new context.
	if(th != NULL) {
		ucontext_t *context = th->ucp;
		free(th);
		setcontext(context);
	}
	pthread_mutex_lock(&threadlock);
	num_of_threads--;
	pthread_mutex_unlock(&threadlock);
}
