/*
 * less_heap.c
 *
 *  Created on: Oct 24, 2013
 *      Author: qianz
 */
#include "less_heap.h"
#include <stdlib.h>
#include <string.h>

thread *pop_min(queue *q) {
	if(q->elements == 0) {
		return NULL;
	}
	thread *ret = q->root[0];
	q->elements--;
	q->root[0] = q->root[q->elements];
	q->root[q->elements] = NULL;
	heapify(q, 0);
	return ret;
}

void insert(thread *node, queue *q) {
	if(q->elements == q->size) {
		q->size *= 2;
		q->root = (thread **)realloc(q->root, q->size * sizeof(thread *));
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &(node->t));
	q->root[q->elements] = node;
	q->elements++;
	int cur = q->elements - 1;
	while(cur != 0 && q->root[cur]->priority < q->root[(cur - 1) / 2]->priority) {
		thread *parent = q->root[(cur - 1) / 2];
		q->root[(cur - 1) / 2] = q->root[cur];
		q->root[cur] = parent;
		cur = (cur - 1) / 2;
	}
}

void init(queue *q) {
	q->size = 10;
	q->elements = 0;
	q->root = (thread **)malloc(q->size * sizeof(thread *));
}

void heapify(queue *q, int i) {
	int left = 2 * i + 1;
	int right = 2 * i + 2;
	int smallest;
	if(left < q->elements && (q->root[left]->priority < q->root[i]->priority ||
	(q->root[left]->priority == q->root[i]->priority && q->root[left]->t.tv_nsec < q->root[i]->t.tv_nsec)))
		smallest = left;
	else
		smallest = i;
	if(right < q->elements && (q->root[right]->priority < q->root[smallest]->priority ||
	(q->root[right]->priority == q->root[smallest]->priority && q->root[right]->t.tv_nsec < q->root[smallest]->t.tv_nsec)))
		smallest = right;
	if(smallest != i) {
		thread *cur = q->root[i];
		q->root[i] = q->root[smallest];
		q->root[smallest] = cur;
		heapify(q, smallest);
	}
}
