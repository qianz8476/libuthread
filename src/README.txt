To compile the example, just type in make (Using included Makefile).
In example test, mutex lock was added to ensure thread safe operations on variables.

When using this library with other code. Please compile with
the following library dependencies:
-lpthread
-lrt

This library depends on pthread for mutex locks and rt for timer functions.
